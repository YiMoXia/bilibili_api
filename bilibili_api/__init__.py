import asyncio
from modulefinder import Module
import platform

import nest_asyncio

from .utils.aid_bvid_transformer import aid2bvid, bvid2aid
from .utils.Credential import Credential
from .utils.Danmaku import Danmaku
from .utils.network import get_session, request, set_session
from .utils.short import get_real_url
from .utils.sync import sync

# 支持嵌套事件
nest_asyncio.apply()

# 如果系统为 Windows，则修改默认策略，以解决代理报错问题
if "windows" in platform.system().lower():
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
